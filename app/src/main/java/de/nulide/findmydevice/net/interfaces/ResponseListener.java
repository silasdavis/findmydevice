package de.nulide.findmydevice.net.interfaces;

import com.android.volley.Response;

import org.json.JSONObject;

public interface ResponseListener extends Response.Listener<JSONObject> {
}
